# ----------------------- Makefile -------------------------------------------------------------------
# Will deploy a given instance/server with asterisk, call-control, cAdvisor
# ----------------------- SETUP -------------------------------------------------------------------
# Will intall all the infrastructure required in AWS LS
infra:
	@echo "== Installing Infrastructure =="
	@./scripts/terraform-deploy.sh

# Will deploy Asterisk, cAdvisor and others in a given instance/server
platform:
	@echo "== Installing Platform and Deploy Asterisk, cAdvisor and Others =="
	@./scripts/ansible-playbooks.sh ubuntu

# Will deploy Asterisk, cAdvisor and others in a given instance/server
platform-vagrant:
	@echo "== Installing Platform and Deploy Asterisk, cAdvisor and Others =="
	@./scripts/ansible-playbooks.sh vagrant

deploy:
	cd ansible && \
	ansible-playbook -v --diff playbook.yml --tags deploy -i dev --limit='playground' --private-key="../mykey" -u ubuntu

deploy-vagrant:
	cd ansible && \
	ansible-playbook -v --diff playbook.yml --tags deploy -i dev --limit='playground' --private-key="../mykey" -u vagrant

# Will remove all the resources deployed in AWS
destroy:
	@echo "== Destroy All Deployed Resources =="
	@./scripts/terraform-destroy.sh
	@./scripts/clean-up.sh

# Will install a server using vagrant in local VirtualBox
infra-vagrant:
	@echo "== Installing Infrastructure with Vagrant =="
	@./scripts/vagrant-deploy.sh

destroy-vagrant:
	@echo "== Destroy All Deployed Resources in Vagrant =="
	@./scripts/vagrant-destroy.sh
	@./scripts/clean-up.sh

create-ssh-key:
	@echo "create key here"
	@ssh-keygen -t rsa -N "" -f mykey

clean-up:
	@echo "cleaning previous run if any"
	@./scripts/clean-up.sh

install: clean-up create-ssh-key infra platform

install-vagrant: clean-up create-ssh-key infra-vagrant platform-vagrant
