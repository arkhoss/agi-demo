#!/bin/bash

# all required by google agi text to speech
sudo yum install perl-libwww-perl
sudo yum install perl-DBI
sudo yum install perl-DBD-MySQL
sudo yum install perl-GD
sudo yum install perl-CGI
sudo yum install perl-JSON
sudo yum install sox
sudo yum install sox-devel
sudo yum install perl-LWP-Protocol-https

