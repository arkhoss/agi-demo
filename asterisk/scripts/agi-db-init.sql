-- MySQL doc-agenda
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema agi_test
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `agi_test` ;

-- -----------------------------------------------------
-- Schema agi_test
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `agi_test` DEFAULT CHARACTER SET utf8 ;
USE `agi_test` ;

-- -----------------------------------------------------
-- Table `agi_test`.`asegur`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `agi_test`.`asegur` ;

CREATE TABLE IF NOT EXISTS `agi_test`.`asegur` (
  `id` INT NULL,
  `cedula` INT NULL,
  `nombre` VARCHAR(45) NULL,
  `apellido` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agi_test`.`clinica`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `agi_test`.`clinica` ;

CREATE TABLE IF NOT EXISTS `agi_test`.`clinica` (
  `id` INT NULL,
  `cedula` INT NULL,
  `nombre` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL,
  `number` VARCHAR(45) NULL,
  `date` VARCHAR(45) NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agi_test`.`pediatria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `agi_test`.`pediatria` ;

CREATE TABLE IF NOT EXISTS `agi_test`.`pediatria` (
  `id` INT NULL,
  `cedula` INT NULL,
  `nombre` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL,
  `number` VARCHAR(45) NULL,
  `date` VARCHAR(45) NULL)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- User `agi_test`.`agi`
-- -----------------------------------------------------
CREATE USER 'agi' IDENTIFIED BY 'docagenda';
GRANT ALL privileges ON `agi_test`.* TO 'agi'@localhost IDENTIFIED BY 'docagenda';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'agi'@localhost;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
