resource "aws_lightsail_key_pair" "agi-demo-key-pair" {
  name       = "AGI-DEMO-KEY"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

resource "aws_lightsail_instance" "agi-demo" {
  name              = "agi-demo"
  availability_zone = var.AVAILABILITY_ZONE
  blueprint_id      = var.BLUEPRINT_ID
  bundle_id         = var.BUNDLE_ID
  key_pair_name     = aws_lightsail_key_pair.agi-demo-key-pair.name
  tags              = var.TAGS

  provisioner "local-exec" {
    command = "echo ${aws_lightsail_instance.agi-demo.public_ip_address} >> ../server_ip"
  }
}
