terraform {
  backend "s3" {
    bucket = "dc-dev-terraform"
    key    = "terraform/agi-demo"
    region = "us-east-1"
 }
}
