
AWS_REGION="us-east-1"
AVAILABILITY_ZONE="us-east-1c"
BLUEPRINT_ID="ubuntu_20_04"
BUNDLE_ID="small_2_0"

TAGS = {
    Project = "agi-demo"
}
