# How to Install

## Prerequisites

1. UNIX/Linux Env or Microsoft WSL
2. Git
3. An AWS Account with AWS LightSail Permissions
4. AWS CLI Credentials
5. AWS CLI
6. Terraform
7. Ansible
8. Text Editor (Recommended VSC)

## What is going to Happen?

At the end of this demo, you will have an AWS LightSail instance running a docker service with fully deployed asterisk and cAdvisor, both as docker containers, With the possibility to run any dockerized application.

## How to Deploy

Clone the repository agi-demo

    ~$ git clone git clone git@bitbucket.org:arkhoss/agi-demo.git && cd agi-demo

Install

    ~$ make install

##  Check and Usage


Go to http://<InstanceIP>:<Port>/

Ports:

Asterisk: 5060
Swagger: 1337
cAdvisor: 8080

## How to Destroy

    ~$ make destroy
