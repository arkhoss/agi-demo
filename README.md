# Deployment A Asterisk server over AWS using Terraform

For our project we used 2 different techniques to provision our server.

### First:

Requirements:

- AWS Account 
- AWS CLI v2
- Terraform v1.0.1
- Ansible

Our first provisioning use Terraform to provision the instance over AWS Lightsail service. After the instance is created then we use Ansible to provision the Platform. Our Platform deploy a set of Docker containers as follow:

- CAdvisor
- Asterisk
- Swagger
- Call-control

### Second:

Requirements:

- Virtualbox
- Vagrant
- Ansible

Our second way of provisioning the solution use Vagrant and Virtualbox to create a Virtual Machine locally. After the virtual machine is created we use use Ansible to provision the Platform. Our Platform deploy a set of Docker containers as described in our first deployment scenario.

### Solution Architecture

![Node Deploy](docs/voip_diagrama.jpg)

-----

### Ansible Requisites

Things you'll need locally for Ansible

- An [ssh key](https://help.github.com/articles/generating-an-ssh-key/)
- [Python 3](https://www.python.org/downloads/) installed. On Mac I use [homebrew](https://brew.sh/).

- Use pip to install Ansible.

  ```
  $ pip install ansible
  ```

### Mac Users
- Ensure you have homebrew installed.

    ```
    $ /usr/bin/ruby -e (curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)
    ```

### Deploying the solution

- To provision in AWS:

  ```
  $ make install
  ```

- To provision in Vagrant:

  ```
  $ make install-vagrant
  ```


- Once deployed to, you can ssh into it:

  ```
  $ ssh <ubuntu or vagrant>@$(cat server_ip)
  ```

- To upgrade the asterisk box or agi+ari application:

  ```
  $ make deploy
  ```
  
  if Vagrant


  ```
  $ make deploy-vagrant
  ```
  

### Accessing the system

Once deployed, a few services will be available on the vm:

- cAdvisor will be running, providing monitoring for the system and
containers. Access this at
[http://< server_ip >:8080/containers/](http://< server_ip >:8080/containers/)

- SwaggerUi will be running at
[http://< server_ip >:1337](http://< server_ip >:1337) pointing at Asterisk.

- Asterisk will be running. Point a sip client at < server_ip > using the
credentials from [asterisk/conf/pjsip.conf](asterisk/conf/pjsip.conf).

- The 'call-control' node app will be running, but it only interacts with Asterisk
and exposes no public facing ui.

- To get inside of Asterisk CLI, ssh into the box and:
  ```
  $ docker exec -ti asterisk asterisk -rvvv
  ```

- to view the container status
  ```
  $ docker ps -a
  ```

- to view logs of any individual container.

  ```
  $ docker logs <container name>
  ```



### Deploying the solution


- to destroy in AWS

  ```
  $ make destroy
  ```

- to destroy in Vagrant

  ```
  $ make destroy-vagrant
  ```
