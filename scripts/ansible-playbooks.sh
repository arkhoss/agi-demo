#!/bin/bash
# Usage: terraform-deploy
# Author: David Caballero <d@dcaballero.net>
# Author: Sebastian Muñoz <sebastiana.munoz@udea.edu.co>
# Version: 1.0

# safe pipefail
set -euo pipefail

SERVER_IP=$(cat server_ip)

echo "
[playgrounds]
playground ansible_host=${SERVER_IP}
" > ansible/dev

echo "Ansible Inventory"
cat ansible/dev

echo "Key permissions"
chmod 600 mykey

cd ansible

sed -i -e "s/{{ INSTANCE_IP_ADDRESS }}/${SERVER_IP}/g" playbook.yml

echo "Sleeping 1min"
sleep 1m

echo "Ansible - Installing Platform"
ansible-playbook -v --diff playbook.yml -i dev --limit='playground' --private-key="../mykey" -u ${1}

cd ..

echo "Platform Installed"
