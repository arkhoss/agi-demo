#!/bin/bash
# Usage: terraform-deploy
# Author: David Caballero <d@dcaballero.net>
# Author: Sebastian Muñoz <sebastiana.munoz@udea.edu.co>
# Version: 1.0

# safe pipefail
set -euo pipefail

echo "Cleaning all!"

echo "" > ansible/dev

sed -i -e 's/192\.168\.68\.68/\{{ INSTANCE_IP_ADDRESS }}/' ansible/playbook.yml

git checkout -- ansible/playbook.yml

rm -rf mykey*

rm -rf server_ip
