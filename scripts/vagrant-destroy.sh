#!/bin/bash
# Usage: vagrant-deploy
# Author: David Caballero <d@dcaballero.net>
# Author: Sebastian Muñoz <sebastiana.munoz@udea.edu.co>
# Version: 1.0

# safe pipefail
set -euo pipefail

pwd

vagrant status

vagrant destroy -f

vagrant status

echo "Vagrant Destroyed"
