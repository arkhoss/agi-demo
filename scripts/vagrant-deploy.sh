#!/bin/bash
# Usage: vagrant-deploy
# Author: David Caballero <d@dcaballero.net>
# Author: Sebastian Muñoz <sebastiana.munoz@udea.edu.co>
# Version: 1.0

# safe pipefail
set -euo pipefail

pwd

mkdir -p src
cp -r mykey.pub src/

vagrant status

vagrant up

vagrant status

echo "Vagrant Completed"

echo "192.168.68.68" > server_ip
